package broncos.boisestate.edu.bsuawesome

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val helloWorldTextView:TextView = findViewById(R.id.helloworld_textview)

        var someInt = 5
        someInt = 4


        val submitButton = findViewById<Button>(R.id.submit_button)

        submitButton.setOnClickListener {
            Log.d("BSU", "This button was tapped")

            helloWorldTextView.text = "This is the changed text"
        }



    }
}
